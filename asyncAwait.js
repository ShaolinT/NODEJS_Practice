const fs = require('fs');


const promiseObj =
    function (path) {
        return new Promise((resolve, reject) => {
            fs.readFile(path, 'utf8', function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            })

        })
    }

//ES6
const start = async()=>{
    try {
        const first = await promiseObj('./hoola.txt');
        console.log(first);
    } catch (error) {
        console.log(error)
    }
}

start();

// //Without ES6    
// async function start() {

//     try {
//         const first = await promiseObj('./hoola.txt');
//         console.log(first);

//     } catch (error) {
//         console.log(error);
//     }

// }

//Without Try Catch
// const start = async()=>{
//     const first = await promiseObj('./hoola.txt');
//     console.log(first);
// }

//Using .Then .Catch
// promiseObj('./hoola.txt')
//     .then((result)=>console.log(result))
//     .catch((err)=>console.log(err))



