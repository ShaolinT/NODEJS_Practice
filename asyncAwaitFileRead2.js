//Without Util.Promisify

const fs = require('fs').promises

const start = async(path)=>{
    try {
        const first = await fs.readFile('./hoola.txt','utf8')
        console.log(first);
        
    } catch (error) {
        console.log(error);
    }
   
}

start();


// //USING UTIL.promisify

// const fs = require('fs');
// const util = require('util')
// const readFilePromise = util.promisify(fs.readFile)
// const writeFilePromise = util.promisify(fs.writeFile)

// const newPromise = async (path) => {
//   try {
//     const first = await readFilePromise('./hoola.txt','utf8')
//     console.log(first);
//   } catch (error) {
//     console.log(error);
//   }
  
// }

// newPromise();


