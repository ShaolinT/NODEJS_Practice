const express = require('express')
const app = express()
const mysql = require('mysql')

// Create Connection

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
   // port: '5002',
    //database: 'sampledb'

})

//connect

db.connect((error) => {
    if (error) {
        throw error
    }
    else
        console.log('mysql connected')
})

app.get('/hi',(req,res)=>{
    console.log('Yo')
    res.send('Hi')
})

//Create Database

app.get('/createdb',(req,res)=>{
    let sql = 'CREATE DATABASE new_db' 
    db.query(sql,(err,result)=>{
        if(err){
            throw err
        }
        console.log(result)
        res.send('Database Created...')
    })
})

// Create/Post Table
app.get('/createtable', (req, res) => {
    let sql = 'CREATE TABLE post(id int AUTO_INCREMENT , title VARCHAR(255), body VARCHAR(255), PRIMARY KEY(id))';
    db.query(sql, (err, result) => {
        if (err) { throw err; }


        console.log(result);

        res.send('Post Table Created ....');
    })
})




app.listen('5002', () => {
    console.log('server is listening to port 5002')
})