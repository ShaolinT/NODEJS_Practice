//setTimeout Asynchronus

console.log("First");

setTimeout(()=>{
    console.log("Second");
},2000)

console.log("Third");

//setInterval Asynchronus Incremental

console.log("First");

setInterval(function(){
    console.log("Second");
},3000)

console.log("Third");