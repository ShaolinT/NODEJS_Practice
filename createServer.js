const http = require('http')

const server = http.createServer((req,res)=>{
if(req.url ==='/'){
    res.end(`Home Page`)
}
else if (req.url==='/about'){
    res.end(`About Page`);
}
else res.end(`Error Page`);

})

server.listen(5000, ()=>{
    console.log('server is listening to port 5000...');
})
