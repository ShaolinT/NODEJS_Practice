const fs = require('fs');


const promiseObj =
    function (path) {
      return  new Promise((resolve, reject) => {
            fs.readFile(path, 'utf8', function (err,data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            })
            
        })
    }

promiseObj('./hoola.txt').then((result)=>console.log(result)).catch((err)=>console.log(err))